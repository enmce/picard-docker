FROM ubuntu:14.04

MAINTAINER "Viktor Svekolkin" <enmce@yandex.ru>

RUN apt-get update && apt-get install -y \
	unzip \
	openjdk-7-jre \
	r-base

#Copy Picard tools archive
ADD picard-tools-1.138.zip /opt/picard-tools-1.138.zip

WORKDIR /opt

#Unzip archive and remove it
RUN unzip picard-tools-1.138.zip && \
	rm picard-tools-1.138.zip

WORKDIR /opt/picard-tools-1.138

#Set entrypoint on script
ENTRYPOINT ["java", "-Xmx4g", "-jar", "picard.jar"]




	
