#Picard Tools docker image

This repository contains Dockerfile that packages Picard Tools 1.138 Docker image.

Picard Tools 1.138 can be downloaded and documentation can be found [here](http://broadinstitute.github.io/picard/).


### The recommended way to build an image is:

```
docker build -t picard:1.138 .
```

### To run Picard Tools 1.138 in container:
If read file <name>.fq is in ~/path_to_reads/, following command will produce output file <name>.sam in directory ~/path_to_reads. 
```
docker run -v ~/path_to_reads:/mnt -u $(echo $UID) --rm picard:1.138 FastqToSam FASTQ=/mnt/<name>.fq SAMPLE_NAME=<name> OUTPUT=/mnt/<name>.sam
```
